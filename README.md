# Medusa library

Paper about the Medusa library.

The `medusa.tex` contains the text of the paper, with the accompanying images being located in the
`images/` folder and the references in `references.bib` file.

The folder `analysis/` contains the source code, parameters and plot scripts for all the examples in
the paper. See the README in that folder for reproduction of the results.
