# Analyses

This folder contains the source code for all the examples in the paper.
To run, you need to install Medusa
[https://gitlab.com/e62Lab/medusa](https://gitlab.com/e62Lab/medusa)
and the accompanying build tools.

Edit the `CMakeLists.txt` file to point to your Medusa installation.

To compile the examples, create a build folder, run `cmake` and then compile the desired example.
```
mkdir build
cd build
cmake ..
make demo2d  # or any other example
```

Once compiled, the executable should appear in the (current) `build/` folder.
It can be run from anywhere, but the simplest is to go back to the analysis folder and run from
there.
```
cd ..
./build/demo2d  # or any other example
```
Some examples also take the parameters file, located in the `params/` folder. In any of the
exmaples, your desired output folder will probably be different from the existing one, so you can
alter it to your desired location. Default settings are a bit different for each case, thermofluid
examples write the data to the working directory, while other have paths specified in settings or
in the beginnin of the main function.

After running, you will get a `.h5` file containing the results. To process and plot the results,
Matlab scripts in `process/` and `plotscripts/` folders can be used. Fist, recursicely add all the
supporting scripts in the `common/` directory using
```
addpath(genpath('common'))
```
Then, while keeping the working directory in the `analyses/` folder, run the process script (if it
exists) and then the plot script corresponding to your example. Some examples are smaller, and only
have the plot script.

Plot scripts can be changed without re-running the whole chain, as the intermediate plot data is
stored in the `plotdata/` folder.

## Benchmarks
Code for running benchmarks is present in the `run/` folder. The processing and plotting process in
the same as described above.

