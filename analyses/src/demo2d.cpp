#include <medusa/Medusa.hpp>

using namespace mm;
using namespace std;

int main(int argc, char* argv[]) {
    typedef Vec2d vec_t;

    HDF domain_file("/mnt/data/ijs/papers/2019_P_Medusa/analyses/domains.h5", HDF::READONLY);
    auto d = DomainDiscretization<vec_t>::load(domain_file, "domain1");
    int N = d.size();

    Range<int> interior = d.interior();
    Range<int> types(N);
    for (int i = 0; i < N; ++i) {
        if (d.type(i) < 0) {
            if ((d.pos(i) - Vec2d(0.95, 0.34)).norm() < 0.4) {
                types[i] = -2;
            } else {
                types[i] = -1;
            }
        }
    }
    Range<int> circle = (types == -2);
    Range<int> boundary = (types == -1);

    d.findSupport(FindClosest(15));
    FindClosest ss(15); ss.forNodes(circle).searchAmong(interior).forceSelf(true);
    d.findSupport(ss);  // more complex stencil selection

    WLS<Gaussians<Vec2d>, GaussianWeight<Vec2d>, ScaleToClosest> approx({9, 100.0}, 1.0);

    auto storage = d.computeShapes<sh::lap|sh::d1>(approx);
    auto op = storage.explicitOperators();

    ScalarFieldd u1(N), u2(N);
    u1.setConstant(0);
    for (int i : boundary) {
        u1[i] = u2[i] = d.pos(i, 0);
    }
    double dt = 1e-4, max_t = 2.5;
    int t_steps = max_t / dt;
    for (int t = 0; t < t_steps; ++t) {
        for (int i : interior) {
            u2[i] = u1[i] + dt*(op.lap(u1, i) + 5.0);
        }
        for (int i : circle) {
            u2[i] = op.neumann(u1, i, d.normal(i), 0.0);
        }
        u2.swap(u1);
    }

    HDF hdf("/mnt/data/ijs/papers/2019_P_Medusa/analyses/demo2d_wip.h5", HDF::DESTROY);
    hdf.writeIntArray("int", interior);
    hdf.writeIntArray("bnd", boundary);
    hdf.writeIntArray("crc", circle);
    hdf.writeDomain("domain", d);
    hdf.writeEigen("solution", u1);
    return 0;
}
