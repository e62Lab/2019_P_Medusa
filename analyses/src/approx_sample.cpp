#include <medusa/Medusa.hpp>

using namespace mm;
using namespace std;
using namespace Eigen;


int main(int argc, char* argv[]) {
    RBFFD<Gaussian<double>, Vec2d, ScaleToClosest, PartialPivLU<MatrixXd>> approx1(100.0);
    RBFFD<Gaussian<double>, Vec2d, NoScale, PartialPivLU<MatrixXd>> approx2(5.0);
    RBFFD<Gaussian<double>, Vec2d, NoScale, JacobiSVDWrapper<double>> approx3(5.0);
    WLS<Monomials<Vec2d>, GaussianWeight<Vec2d>, ScaleToClosest> approx4(2, 1.0);
    RBFFD<Polyharmonic<double, 5>, Vec2d, ScaleToClosest, PartialPivLU<MatrixXd>> approx5({}, 2);
    return 0;
}
