struct VisibilityStencil {
    int num_closest;
    explicit VisibilityStencil(int num_closest) : num_closest(num_closest) {}

    /// Check for straight-line visibility by testing intermediate points with some density.
    template <typename vec_t>
    bool visible(const vec_t& p, const vec_t& q, const DomainDiscretization<vec_t>& domain) const {
        int density = 100;
        vec_t increment = (q-p) / density;
        for (int i = 1; i < density; ++i) {
            if (!domain.contains(p + i*increment)) return false;
        }
        return true;
    }

    /// Method responsible for stencil construction.
    template <typename vec_t>
    void operator()(DomainDiscretization<vec_t>& domain) const {
        int N = domain.size();
        KDTree<vec_t> tree(domain.positions());
        for (int i = 0; i < N; ++i) {
            Range<int> indices = tree.query(domain.pos(i), num_closest).first;
            for (int j : indices) {
                if (visible(domain.pos(i), domain.pos(j), domain)) {
                    domain.support(i).push_back(j);
} } } } };
