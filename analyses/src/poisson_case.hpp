#ifndef ERRORINDICATOR_POISSON_CASE_HPP
#define ERRORINDICATOR_POISSON_CASE_HPP

#include <utility>
#include <array>
#include <medusa/Medusa_fwd.hpp>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>
#include <Eigen/LU>

using namespace mm;
using namespace Eigen;
using namespace std;

template<typename vec_t>
class Solver {
  public:
    typedef typename vec_t::scalar_t scalar_t;
    enum { dim = vec_t::dim };
    UniformShapeStorage<vec_t> storage;

  private:
    template <typename approx_t>
    ScalarField<typename vec_t::scalar_t> solve_(
            const XML& conf, DomainDiscretization<vec_t>& domain, HDF& file, Timer& timer,
            const approx_t& approx) {
        typedef typename vec_t::scalar_t scalar_t;
        int N = domain.size();
        prn(N);
        int n = conf.get<int>("approx.n");
        domain.findSupport(FindClosest(n));

        prn("shapes");
        timer.addCheckPoint("shapes");
        indexes_t all = domain.all();
        computeShapes(domain, approx, all, std::tuple<Lap<dim>>(), &storage);
        timer.addCheckPoint("matrix");
        SparseMatrix<scalar_t, RowMajor> M(N, N);
        Eigen::VectorXd rhs = Eigen::VectorXd::Zero(N);

        std::vector<std::string> as = split(conf.get<std::string>("case.a"), ',');
        Eigen::Array<scalar_t, vec_t::dim, 1> a;
        for (int d = 0; d < vec_t::dim; ++d) a[d] = std::stod(as[d]);
        auto op = storage.implicitOperators(M, rhs);

        M.reserve(storage.supportSizes());
        for (int i : domain.interior()) {
            scalar_t r = -PI * PI * a.square().sum() * (PI * a * domain.pos(i).array()).sin().prod();
            op.lap(i) = r;
        }
        for (int i : domain.boundary()) {
            op.value(i) = (PI * a * domain.pos(i).array()).sin().prod();
        }

        timer.addCheckPoint("compute");

        SparseLU<SparseMatrix<scalar_t>> solver;
        prn("compute");
        solver.compute(M);
        prn("solve");

        timer.addCheckPoint("solve");
        VectorXd sol = solver.solve(rhs);

        file.atomic().writeEigen("sol", sol);

        return sol;
    }

  public:
    ScalarField<typename vec_t::scalar_t>
    solve(DomainDiscretization<vec_t>& domain, XML& conf, HDF& file, Timer& timer) {
        std::vector<std::string> basis_names = {"gau", "mq", "imq", "mon", "mont", "phs", "gaufixed", "gaufixedjac"};
        auto basis = conf.get<std::string>("approx.basis_type");
        assert_msg((std::find(basis_names.begin(), basis_names.end(), basis) != basis_names.end()),
                   "Basis name '%s' not among valid basis names: %s.", basis, basis_names);

        double sigmaB = conf.get<double>("approx.sigmaB");
        double sigmaW = conf.get<double>("approx.sigmaW");
        std::vector<std::string> augs = split(conf.get<string>("approx.aug"), ',');
        std::vector<std::string> ns = split(conf.get<std::string>("approx.ns"), ',');
        if (basis == "gau") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<Gaussian<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine);
        } else if (basis == "gaufixed") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<Gaussian<double>, vec_t, NoScale, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine);
        } else if (basis == "gaufixedjac") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<Gaussian<double>, vec_t, NoScale, JacobiSVDWrapper<double>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine);
        } else if (basis == "mq") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<Multiquadric<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine);
        } else if (basis == "phs") {
            ScalarField<typename vec_t::scalar_t> q;

            for (int i = 0; i < augs.size(); ++i) {
                int aug = std::stoi(augs[i]);
                int n = std::stoi(ns[i]);
                conf.set("approx.n", n, true);
                string pname = file.groupName();
                file.setGroupName(pname+format("/aug%+d", aug));
                int k = conf.get<int>("approx.k");
                RBFFD<Polyharmonic<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(k, aug);
                Timer timer1;
                q = solve_(conf, domain, file, timer1, engine);

                file.setGroupName(pname);
            }
            return q;

        } else if (basis == "imq") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<InverseMultiquadric<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine);
        } else if (basis == "mon") {
            int m = conf.get<int>("approx.m");
            WLS<Monomials<vec_t>, GaussianWeight<vec_t>, ScaleToClosest> wls(Monomials<vec_t>(m), sigmaW);
            return solve_(conf, domain, file, timer, wls);
        } else if (basis == "mont") {
            int m = conf.get<int>("approx.m");
            WLS<Monomials<vec_t>, GaussianWeight<vec_t>, ScaleToClosest> wls(Monomials<vec_t>::tensorBasis(m), sigmaW);
            return solve_(conf, domain, file, timer, wls);
        }
        throw std::runtime_error("Invalid basis.");
    }
};


#endif  // ERRORINDICATOR_POISSON_CASE_HPP
