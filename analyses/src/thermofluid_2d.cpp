#include <medusa/Medusa_fwd.hpp>
#include <medusa/bits/domains/BasicRelax.hpp>
#include <Eigen/SparseCore>
#include <Eigen/IterativeLinearSolvers>
#include <Eigen/SparseLU>

#include "overseer.hpp"
//#include "medusa/bits/io/HDF_Eigen.hpp"

#define EPS 0.0000001 //small number
#define _MAX_ITER_ 1500

/// Basic medusa example -- heat transfer from heated cylinder -- Demirdzic case
/// http://www-e6.ijs.si/medusa/wiki/index.php/Natural_convection_from_heated_cylinder
using namespace mm;
typedef Vec2d vec_t;
typedef double scal_t;
// support vars
Overseer<scal_t, vec_t> O;
std::chrono::high_resolution_clock::time_point time_1, time_2;


int step_history, i_count_cumulative;

template <typename T, typename u>
bool status_report(int time_step, double dt, double final_time, T& T_2, u& u_2, double C) {
    double time = time_step * dt;
    time_1 = std::chrono::high_resolution_clock::now();
    int d_step = std::floor((final_time / dt) / O.out_no);
    // intermediate IO
    if (time_step % d_step == 0 || time + dt > final_time || time == 0 || O.out_no<0) {
        O.hdf_out.reopenFile();
        O.hdf_out.openGroup("/step" + std::to_string(O.out_recordI));
        O.hdf_out.writeDoubleAttribute("time", time);
        O.hdf_out.writeDoubleArray("T", T_2);
        O.hdf_out.writeEigen("v", u_2);
        O.hdf_out.close();
        O.out_recordI++;
    }
    // on screen reports
    if ((O.on_screen > 0.0 && std::chrono::duration<double>(time_1 - time_2).count() > O.on_screen)
        || time == 0 || time >= final_time) {
        double time_elapsed = (O.timings.durationToNow("time loop"));
        double time_estimated = time_elapsed * (final_time / (time + EPS));
        //peclet
        //Range<double> Pe;
        //for (int i:air_all) Pe.push_back(domain.fill_density(domain.air.positions[i]) * v_2[i].norm());

        std::cout.setf(std::ios::fixed, std::ios::floatfield);
        std::cout // << "#:" << step << "-" << std::setprecision(1)
                << std::setprecision(0)
                << (double) time / final_time * 100 << " % "
                << "e[m]:" << time_elapsed / 60 << std::setprecision(0) << "-"
                << time_estimated / 60
                << std::setprecision(2)
                << " T:" << *std::max_element(T_2.begin(), T_2.end()) << " "
                << *std::min_element(T_2.begin(), T_2.end()) << " "
                << std::accumulate(T_2.begin(), T_2.end(), 0.0) / static_cast<double>(T_2.size())
                << " "
                << "mean u:" << u_2.mean()
                << " C:" << C << " "
                << " PV:" << static_cast<double> (i_count_cumulative) / (time_step - step_history)
                << std::endl;
        time_2 = time_1;
        i_count_cumulative = 0;
        step_history = time_step;
        if (std::isnan(*std::max_element(T_2.begin(), T_2.end()))) return true;
    }
    return false;
}
// 0 - WLS<monomials>,  1 - WLS<gauss>, 2 - RBFFD<PHS>, 3 - RBFFD<Gauss>
// 4 - WLS<MQ>, 5 - RBFFD<MQ>
#define APPX_ENG 2

int main(int arg_num, char* arg[]) {
    /// INIT
    ////////////////////////////////////////////////////////////////////////////////////////////////
    O(arg_num, arg);
    O.timings.addCheckPoint("init");
    // case setup
    scal_t dt = O.xml.get<double>("num.dt");
    scal_t dl = O.xml.get<double>("num.dl");
    scal_t dl_2 = O.xml.get<double>("num.dl_2");
    scal_t v_ref = O.xml.get<double>("num.v_ref");
    scal_t max_div = O.xml.get<double>("num.max_div");


    scal_t time = O.xml.get<double>("phy.time");
    int n = O.xml.get<int>("mls.n");
    int m = O.xml.get<int>("mls.m");
    scal_t sigma_b = O.xml.get<double>("mls.sigma_b");
    scal_t sigma_w = O.xml.get<double>("mls.sigma_w");
    scal_t dx = O.xml.get<double>("num.d_0");
    scal_t mu = O.xml.get<double>("phy.mu");
    scal_t rho = O.xml.get<double>("phy.rho");
    scal_t lam = O.xml.get<double>("phy.lam");
    scal_t c_p = O.xml.get<double>("phy.c_p");
    scal_t g_0 = O.xml.get<double>("phy.g_0");
    Vec2d g{0, g_0};
    scal_t beta = O.xml.get<double>("phy.beta");
    scal_t T_ref = O.xml.get<double>("phy.T_ref");
    scal_t T_cold = O.xml.get<double>("case.T_cold");
    scal_t T_hot = O.xml.get<double>("case.T_hot");

    double Ra = std::abs(g_0) * beta * rho * rho * c_p * std::pow(1, 3) *
                std::abs(T_hot - T_cold) / (lam * mu);
    prn(Ra);
    std::cout << "Pr = " << mu * c_p / lam << std::endl;
    // scal_t g_0 = O.xml.get<double>("phy.g_0");
    int seed = O.xml.get<int>("domain.seed");
    int fill_samples = O.xml.get<int>("domain.num_samples");
    prn(lam / rho / c_p * dt / dx / dx)
    double Re = rho / mu;
    prn(Re);
    /// END OF INIT
    ////////////////////////////////////////////////////////////////////////////////////////////////
    // DOMAIN
    ////////////////////////////////////////////////////////////////////////////////////////////////
    O.timings.addCheckPoint("domain start");
    BoxShape<vec_t> box(0.0, 1.0);;
    DomainDiscretization<vec_t> domain = box.discretizeBoundaryWithStep(dx);


    if (O.xml.get<bool>("domain.obstacles")) {
        // Obstacles
        Range<double> o_r = {0.24,       0.06,       0.15,           0.05,       0.2,      0.09,        0.09,   0.13};
        Range<vec_t> o_c = {{0.95,0.13}, {0.02,0.5}, {0.55,0.95},    {0.73,0.4}, {0.03,0.0}, {0.39,0.62},{1,1}, {1,0.4}};
        Range<int> o_t =    {-16,       -15,        -14,            -15,        -14,        -16,        -14,    -16};
        // cold side obstacles - 6, hot side obstacles - 5, Neumann side -4
        for (auto i=0; i<o_t.size() ; ++i) {
            BallShape<vec_t> ball(o_c[i], o_r[i]);
            DomainDiscretization<vec_t> obstacle = ball.discretizeBoundaryWithStep(dx);
            obstacle.types() = o_t[i];
            domain.subtract(obstacle);
        }
    }

    // Fill
    if (O.xml.get<bool>("domain.fill")) {
        GeneralFill<vec_t> fill;
        BasicRelax relax;
        relax.initialHeat(O.xml.get<double>("relax.initial_heat"))
                .finalHeat(O.xml.get<double>("relax.final_heat")).iterations(
                        O.xml.get<int>("relax.iter_num"))
                .numNeighbours(O.xml.get<int>("relax.num_neighbours")).projectionType(
                        BasicRelax::DO_NOT_PROJECT);

        fill.seed(seed).numSamples(fill_samples).proximityTolerance(
                O.xml.get<double>("domain.proximity_tol"));
        fill(domain, dx);
        relax(domain);
    } else {
        domain = box.discretizeWithStep(dx);
    }
    // remove corner nodes
    if (O.xml.get<bool>("domain.no_corner")) {
        Range<int> corner = domain.positions().filter([](const vec_t& p) {
            return ((p[0] < EPS && (p[1] < EPS || p[1] > 1 - EPS)) ||
                    (p[0] > 1 - EPS && (p[1] < EPS || p[1] > 1 - EPS)));
        });
        domain.removeNodes(corner);
    }

    Range<int> hot_side_obstacles = domain.types() == -15;
    Range<int> cold_side_obstacles = domain.types() == -16;
    Range<int> neumann_side_obstacles = domain.types() == -14;

    Range<int> left_idx = domain.types() == -1;
    Range<int> right_idx = domain.types() == -2;
    Range<int> top_idx = domain.types() == -3 ;
    Range<int> bottom_idx = domain.types() == -4;
    top_idx = top_idx + neumann_side_obstacles;

    Range<int> interior = domain.interior();
    Range<int> boundary = domain.boundary();

    // ghost nodes
    Range<int> gh(domain.size(), -100);
    if (O.xml.get<bool>("domain.ghost")) {
        for (int i:boundary) {
            gh[i] = domain.addInternalNode(domain.pos(i) + domain.normal(i) * dx, 9);
        }
    }
    int N = domain.size();
    O.hdf_out.reopenFile();
    O.hdf_out.openGroup("/step0");
    O.hdf_out.writeDouble2DArray("positions", domain.positions());
    O.hdf_out.writeIntArray("types", domain.types());
    O.hdf_out.close();
    prn(domain.size());

    // APPROXIMATION
    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Operator discretization
    #if APPX_ENG == 0
    WLS<Monomials<vec_t>, GaussianWeight<vec_t>, ScaleToClosest> appr(m, sigma_w);
    #elif APPX_ENG == 1
    WLS<Gaussians<vec_t>, GaussianWeight<vec_t>, ScaleToClosest> appr({m, sigma_b}, sigma_w);
    #elif APPX_ENG == 2
    Polyharmonic<double, 3> ph;  // construct Polyharmonic
    RBFFD<Polyharmonic<double, 3>, vec_t, ScaleToClosest>
            appr(ph, Monomials<vec_t>(2));
            #elif APPX_ENG == 3
    RBFFD<Gaussian<double>, vec_t, ScaleToClosest> appr(sigma_b, Monomials<vec_t>(0));
    #elif APPX_ENG == 4
    WLS<MQs<vec_t>, GaussianWeight<vec_t>, ScaleToClosest> appr({m, sigma_b});
    #elif APPX_ENG == 5
    RBFFD<Multiquadric<double>, vec_t, ScaleToClosest> appr(sigma_b, Monomials<vec_t>(1));
    #endif

    domain.findSupport(FindClosest(n));

    // FIELD DECLARATIONS
    ////////////////////////////////////////////////////////////////////////////////////////////////
    VectorField2d u_1(domain.size());
    ScalarFieldd p = ScalarFieldd::Zero(domain.size());
    ScalarFieldd T_1(domain.size());
    u_1.setZero();
    T_1.setZero();
    // dirichlet boundary conditions
    T_1 = O.xml.get<double>("case.T_init");
    T_1[left_idx] = T_cold;
    T_1[right_idx] = T_hot;

    ScalarFieldd T_2 = T_1;
    VectorField2d u_2 = u_1;

    O.timings.addCheckPoint("shapes start");

    // OPERATORS
    ////////////////////////////////////////////////////////////////////////////////////////////////
    //pressure correction matrix -- note N + 1, for additional constraint
    Eigen::SparseLU<Eigen::SparseMatrix<double>> solver_p;
    Eigen::SparseMatrix<double, Eigen::RowMajor> M(N + 1, N + 1);
    Eigen::VectorXd rhs(N + 1);

    auto storage = domain.computeShapes<sh::lap | sh::grad>(appr);
    auto op_v = storage.explicitVectorOperators(); // vector operators
    auto op_s = storage.explicitOperators(); // scalar operators
    auto op_i = storage.implicitOperators(M, rhs);
    Range<int> per_row(N+1,n+1);
    per_row[N]=N;
    M.reserve(per_row);

    // pressure correction
    for (int i : interior) {
        op_i.lap(i).eval(1);
    }
    for (int i : boundary) {
        op_i.neumann(i, domain.normal(i)).eval(1);
    }
    //Ghost nodes
    if (O.xml.get<bool>("domain.ghost")) {
        for (int i : boundary) {
            op_i.lap(i, gh[i]).eval(1);
        }
    }

    // regularization
    // set the last row and column of the matrix
    for (int i = 0; i < N; ++i) {
        M.coeffRef(N, i) = 1;
        M.coeffRef(i, N) = 1;
    }
    // set the sum of all values
    rhs[N] = 0.0;
    M.makeCompressed();
    Eigen::SparseMatrix<double> MM(M);
    solver_p.compute(MM);
    // time loop
    O.timings.addCheckPoint("time loop");
    for (int time_step = 0; time_step < std::floor(time / dt); ++time_step) {
        double C;
        int i, i_count;
        if (status_report(time_step, dt, time, T_2, u_2, C)) break;

        // implicit navier stokes
        Eigen::SparseMatrix<double, Eigen::RowMajor> M_v(2 * N, 2 * N);
        Eigen::VectorXd rhs_vec(2 * N);
        Eigen::BiCGSTAB<Eigen::SparseMatrix<double, Eigen::RowMajor>,
                Eigen::IncompleteLUT<double>> solver_u;
        rhs_vec.setZero();

        Range<int> per_row_v(2 * N, n);

        auto op_iv = storage.implicitVectorOperators(M_v, rhs_vec);
        M_v.reserve(per_row_v);

        for (int i : domain.all()) {
            1 / dt * op_iv.value(i) + op_iv.grad(i, u_1[i]) + (-1 * mu / rho) * op_iv.lap(i)
                    = /*-0 / rho * op_s.grad(p, i) +*/ u_1[i] / dt +
                      1 * g * (1 - beta * (T_1[i] - T_ref));
        }

        M_v.makeCompressed();
        solver_u.compute(M_v);
        Eigen::VectorXd solution = solver_u.solveWithGuess(rhs_vec, u_1.asLinear());
        u_2 = VectorField2d::fromLinear(solution);

        // pressure correction
        // P-V correction iteration -- PVI
        for (i_count = 0; i_count < _MAX_ITER_; ++i_count) {
            for (int i : interior) rhs(i) = rho / dt * op_v.div(u_2, i);
            for (int i : boundary) rhs(i) = rho / dt * u_2[i].dot(domain.normal(i));
            //for (int i : ball_idx) rhs(i) = rho / dt * u_2[i].dot(domain.normal(i));

            //Ghost nodes
            if (O.xml.get<bool>("domain.ghost")) {
                for (int i : boundary) {
                    rhs(gh[i]) = 0;
                }
            }

            ScalarFieldd P_c = solver_p.solve(rhs).head(N);

            #pragma omp parallel for private(i) schedule(static)
            for (i = 0; i < interior.size(); ++i) {
                int c = interior[i];
                u_2[c] -= dt / rho * op_s.grad(P_c, c);
            }
            // force BCs
            u_2[boundary] = 0;
            //Ghost nodes
            if (O.xml.get<bool>("domain.ghost")) {
                for (int i : boundary) {
                    u_2(gh[i]).setZero();
                }
            }

            if (rhs.maxCoeff() < max_div || max_div < 0) break;
        }

        i_count_cumulative += i_count;

        // implicit heat transfer
        Eigen::SparseMatrix<double, Eigen::RowMajor> M_T(N, N);
        Eigen::BiCGSTAB<Eigen::SparseMatrix<double, Eigen::RowMajor>,
                Eigen::IncompleteLUT<double>> solver_T;
        Eigen::VectorXd rhs_T(N);
        rhs_T.setZero();
        auto op_t = storage.implicitOperators(M_T, rhs_T);

        Range<int> per_row(N, n);
        M_T.reserve(per_row);
        //eu
        for (int i : interior) {
            op_t.value(i) + (-dt * lam / rho / c_p) * op_t.lap(i) +
            dt * op_t.grad(i, u_2[i]) = T_1(i);
        }
        // BCs
        for (int i : top_idx + bottom_idx) {
            op_t.neumann(i, domain.normal(i)) = 0;
        }

        for (int i : left_idx) op_t.value(i) = T_hot;
        for (int i : right_idx) op_t.value(i) = T_cold;
        for (int i : cold_side_obstacles) op_t.value(i) = T_cold;
        for (int i : hot_side_obstacles) op_t.value(i) = T_hot;


        //Ghost nodes
        if (O.xml.get<bool>("domain.ghost")) {
            for (int i : top_idx + bottom_idx) {
                op_t.value(i, gh[i]) + (-dt * lam / rho / c_p) * op_t.lap(i, gh[i]) +
                dt * op_t.grad(i, u_2[i], gh[i]) = T_1(i);
            }
            for (int i : left_idx) op_t.value(gh[i]) = T_hot;
            for (int i : right_idx) op_t.value(gh[i]) = T_cold;
            for (int i : hot_side_obstacles) op_t.value(gh[i]) = T_hot;
            for (int i : cold_side_obstacles) op_t.value(gh[i]) = T_cold;
        }

        M_T.makeCompressed();
        solver_T.compute(M_T);
        T_2 = solver_T.solveWithGuess(rhs_T, T_1);

        T_1.swap(T_2);
        u_1.swap(u_2);
    }

    O.timings.addCheckPoint("end");
    O.timings.showTimings();

// ---------
    return 0;
}

