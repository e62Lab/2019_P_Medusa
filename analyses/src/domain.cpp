#include <medusa/Medusa.hpp>
#include <medusa/bits/domains/GrainDropFill.hpp>
#include <Eigen/SparseCore>
#include <Eigen/IterativeLinearSolvers>
#include <Eigen/Geometry>

using namespace mm;
using namespace std;
using namespace Eigen;


void domain1(HDF& hdf) {
    PolygonShape<Vec2d> poly({{-1, -1}, {2, -1}, {2, 1}, {1, 0.5}, {-1, 1}});
    BallShape<Vec2d> ball({1, -0.2}, 0.3);
    auto shape = (poly - ball).rotate(PI/6) + BoxShape<Vec2d>({-2, 0}, {-1, 1});
    auto h = [](const Vec2d& p) { return 0.025 + p.norm()/20; };
    DomainDiscretization<Vec2d> domain = shape.discretizeBoundaryWithDensity(h);
    GeneralFill<Vec2d> fill; fill.seed(1);
    fill(domain, h);
    domain.findSupport(FindClosest(12));

    STLShape<Vec3d> stl_shape(STL::read("../data/hip.stl"));
    double dx = 0.05;
    DomainDiscretization<Vec3d> stl_domain = stl_shape.discretizeBoundaryWithStep(dx);
    auto [bot, top] = stl_shape.bbox();
    GrainDropFill<Vec3d> gfill(bot, top);
    gfill.seed(1).initialSpacing(0.01).maxPoints(1e7);
    gfill(stl_domain, dx);

    hdf.atomic().writeDomain("domain1", domain);
    hdf.setGroupName("/domain1");
    hdf.atomic().writeInt2DArray("supp", domain.supports());
    hdf.setGroupName("/");

    hdf.atomic().writeDomain("domain2", stl_domain);
    hdf.setGroupName("/domain2");
    hdf.atomic().writeInt2DArray("supp", stl_domain.supports());
    hdf.setGroupName("/");
}

int main() {
    HDF file("/mnt/data/ijs/papers/2019_P_Medusa/analyses/domains_wip.h5", HDF::DESTROY);
    file.close();
    domain1(file);

    return 0;
}