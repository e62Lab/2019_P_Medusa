#include <medusa/Medusa.hpp>
#include "laplace_approx.hpp"

using namespace mm;
using namespace std;
using namespace Eigen;

template <int dim>
void analyse(const XML& conf) {
    typedef Vec<double, dim> vec;

    auto file = conf.get<std::string>("meta.file");
    HDF hdf(file , HDF::DESTROY);
    hdf.writeXML("conf", conf);
    hdf.close();

    auto sh = BoxShape<vec>(0.0, 1.0);

    vector<string> ns = split(conf.get<string>("case.nxs"), ',');
    for (const string& s : ns) {
        int nx = std::stoi(s);
        
        hdf.setGroupName(format("/n%04d", nx));
        DomainDiscretization<vec> domain = sh.discretizeWithStep(1.0/nx);
        hdf.atomic().writeDomain("domain", domain);

        int n = conf.get<int>("wls.n");
        domain.findSupport(FindClosest(n));

        Timer t;
        Approx<vec> solver;
        solver.solve(domain, conf, hdf, t);
    }
}

int main(int argc, char* argv[]) {
    if (argc < 2) { print_red("Supply parameter file as the second argument.\n"); return 1; }
    XML conf(argv[1]);

    int dim = conf.get<int>("case.dim");
    assert_msg(dim == 1 || dim == 2 || dim == 3, "Dimension %d not supported", dim);
    if (dim == 1) analyse<1>(conf);
    if (dim == 2) analyse<2>(conf);
    if (dim == 3) analyse<3>(conf);

    return 0;
}
