#ifndef EXAMPLES_FLUID_FLOW_OVERSEER_HPP_
#define EXAMPLES_FLUID_FLOW_OVERSEER_HPP_

/**
 * @brief class with all global thingies like params, timers ...
 */
using namespace mm;
template <typename scal_t, typename vec_t>
class Overseer {
  public:
    XML xml;
    HDF hdf_out;

    std::ofstream debug_out;
    std::string hdf_out_filename;
    // Timer
    Timer timings;
    // MLS params
    std::ofstream out_file;
    int out_recordI;

    // io params
    int out_no;  // tno of hfd reports
    scal_t on_screen;  // time between two on screen reports

    Overseer() {};
    ~Overseer() { debug_out.close();}
    template <typename string_array_t>
    void operator()(int argc, string_array_t argv) {
        if (argc < 2) { print_red("Supply the parameters file as the first argument."); }
        //file logic
        std::string xml_fn = argv[1];
        std::string fn = (xml_fn.size() >= 4 && xml_fn.substr(xml_fn.size()-4, 4) == ".xml") ?
                          xml_fn.substr(0, xml_fn.size()-4) : xml_fn;

        //out names is made of executable name and param file name
        std::stringstream ss;
        ss << mm::split(argv[0], '/').back() <<"_"<< mm::split(std::string(fn), '/').back();
        std::string filename = ss.str();
        std::string debug_out_filename = filename + ".m";
        //Set output files
        hdf_out_filename = filename + ".h5";
        std::remove(hdf_out_filename.c_str());  // remove old h5

        hdf_out.openFile(hdf_out_filename, HDF::DESTROY);  // create new blank file, ignore errors
        hdf_out.closeFile();

        debug_out.open(debug_out_filename, std::ofstream::out);  // debug out file
        assert_msg(debug_out.is_open(), "Error opening file '%s': '%s'",
                   debug_out_filename, strerror(errno));

        //load other parameters from xml
        xml.load(xml_fn);

        std::cout << "loading params, using: ";
        print_white(xml_fn + "\n");
        prn(xml);

        on_screen = xml.get<double>("io.on_screen");
        out_no = xml.get<int>("io.out_no");

        //params LOGIC
        omp_set_num_threads(xml.get<int>("sys.num_threads"));

        //std::cout << "Re = " << rho / mu << std::endl;

        /*Ra = std::abs(g_0) * beta * rho * rho * c_p * std::pow(height, 3) *
             std::abs(T_hot - T_cold) / (lam * mu);
        std::cout << "Ra = " << Ra
                  << std::endl;
        std::cout << "Pr = " << mu * c_p / lam << std::endl;
        std::cout << "-----------------------------------------------------------" << std::endl;
        */
        mm::print_green("Params loaded ... \n");
    }
};

#endif  // EXAMPLES_FLUID_FLOW_OVERSEER_HPP_
