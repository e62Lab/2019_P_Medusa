#include <medusa/Medusa.hpp>
#include "Eigen/SparseCore"
#include "Eigen/IterativeLinearSolvers"

#ifdef NDEBUG
    #warning "This is a release build!"
#else
    #warning "This is a debug build!"
#endif


using namespace mm;
using namespace std;
using namespace Eigen;

typedef Vec3d vec;

void solve(double n, HDF& file) {

    Timer t;
    t.addCheckPoint("domain");
    double h = 1.0 / n;
    double r1 = 1.0;
    double r2 = r1/2;
    auto sh = BallShape<vec>(0.0, r1) - BallShape<vec>(0.0, r2);
    DomainDiscretization<vec> domain = sh.discretizeBoundaryWithStep(h);
    GeneralFill<vec> fill; fill.seed(0).numSamples(10);
    KDGrid<vec> grid(-r1, r1, h/2);
    fill(domain, [=](const vec&) { return h; }, grid);
    t.addCheckPoint("support");

    domain.findSupport(FindClosest(35));

    t.addCheckPoint("approx");
    int aug = 2;
    RBFFD<Polyharmonic<double, 3>, vec, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> approx({}, aug);

    int N = domain.size();
    auto storage = domain.computeShapes<sh::lap>(approx, domain.interior());
    t.addCheckPoint("matrix");

    SparseMatrix<double, RowMajor> M(N, N);
    M.reserve(storage.supportSizes());
    Eigen::VectorXd rhs = Eigen::VectorXd::Zero(N);
    auto op = storage.implicitOperators(M, rhs);
    for (int i : domain.interior()) {
        double f = -3*PI * PI * (PI * domain.pos(i).array()).sin().prod();
        op.lap(i) = f;
    }
    for (int i : domain.boundary()) {
        op.value(i) = (PI * domain.pos(i).array()).sin().prod();
    }

    t.addCheckPoint("compute");

    BiCGSTAB<decltype(M), IncompleteLUT<double>> solver;
    solver.setTolerance(1e-6);
    solver.setMaxIterations(100);
    solver.preconditioner().setDroptol(1e-2);
    solver.preconditioner().setFillfactor(5);

//    SparseLU<SparseMatrix<double>> solver;
    solver.compute(M);

    t.addCheckPoint("solve");

    VectorXd sol = solver.solve(rhs);

    t.addCheckPoint("postprocess");
    VectorXd err = sol;
    for (int i = 0; i < N; ++i) {
        err[i] -= (PI * domain.pos(i).array()).sin().prod();
    }
    t.addCheckPoint("end");
    prn(N);
    prn(solver.iterations());
    prn(solver.error());

    file.atomic().writeIntAttribute("N",  N);
    file.atomic().writeDoubleAttribute("err_inf",  err.lpNorm<Infinity>());
    file.atomic().writeDoubleAttribute("err_1",  err.lpNorm<1>());
    file.atomic().writeDoubleAttribute("err_2",  err.lpNorm<2>());
    file.atomic().writeTimer("times", t);

    cout << N << endl;
    cout << err.lpNorm<Infinity>() << endl;
    cout << t << endl;
}

int main(int argc, char* argv[]) {
    if (argc < 2) { print_red("Supply parameter file as the second argument.\n"); return 1; }
    XML conf(argv[1]);
    auto file = conf.get<std::string>("meta.file");
    HDF hdf(file , HDF::DESTROY);
    hdf.writeXML("conf", conf);
    hdf.close();

    vector<string> ns = split(conf.get<string>("case.nxs"), ',');
    for (const string& s : ns) {
        double nx = std::stod(s);
        prn(nx);

        hdf.setGroupName(format("/n%04d", nx));
        solve(nx, hdf);
    }

    return 0;
}
