#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include <Eigen/PardisoSupport>

using namespace mm;
using namespace std;
using namespace Eigen;

struct BeamSolver {

    static void preprocess(XML& conf) {
        double E = conf.get<double>("case.E");
        double nu = conf.get<double>("case.nu");

        // parameter logic
        if (conf.get<string>("case.type") == "plane stress") {
            double mu = E / 2. / (1 + nu);
            double lam = E * nu / (1 - 2 * nu) / (1 + nu);
            conf.set("case.mu", mu);
            conf.set("case.lam", lam);
        }
    }

    template<typename vec_t>
    std::pair<VectorField<double, vec_t::dim>, VectorField<double, vec_t::dim*(vec_t::dim+1)/2>>
    solve(const XML& conf, DomainDiscretization<vec_t>& domain, HDF& file, Timer& timer) {
        std::vector<std::string> basis_names = {"gau", "mq", "imq", "mon", "mont", "phs", "gaufixed", "gaufixedjac"};
        auto basis = conf.get<std::string>("approx.basis_type");
        assert_msg((std::find(basis_names.begin(), basis_names.end(), basis) != basis_names.end()),
                   "Basis name '%s' not among valid basis names: %s.", basis, basis_names);

        double sigmaB = conf.get<double>("approx.sigmaB");
        double sigmaW = conf.get<double>("approx.sigmaW");
        std::vector<std::string> augs = split(conf.get<string>("approx.aug"), ',');
        if (basis == "gau") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<Gaussian<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine);
        } else if (basis == "gaufixed") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<Gaussian<double>, vec_t, NoScale, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine);
        } else if (basis == "gaufixedjac") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<Gaussian<double>, vec_t, NoScale, JacobiSVDWrapper<double>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine);
        } else if (basis == "mq") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<Multiquadric<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine);
        } else if (basis == "phs") {
            std::pair<VectorField<double, vec_t::dim>, VectorField<double, vec_t::dim*(vec_t::dim+1)/2>> q;
            for (auto s : augs) {
                int aug = std::stoi(s);
                string pname = file.groupName(); if (pname == "/") pname = "";
                file.setGroupName(pname+format("/aug%+d", aug));
                int k = conf.get<int>("approx.k");
                RBFFD<Polyharmonic<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(
                        k, aug);
                Timer timer1;
                q = solve_(conf, domain, file, timer1, engine);
                if (pname.empty()) pname = "/";
                file.setGroupName(pname);
            }
            return q;

        } else if (basis == "imq") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<InverseMultiquadric<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine);
        } else if (basis == "mon") {
            int m = conf.get<int>("approx.m");
            WLS<Monomials<vec_t>, GaussianWeight<vec_t>, ScaleToClosest> approx(Monomials<vec_t>(m), sigmaW);
            return solve_(conf, domain, file, timer, approx);
        } else if (basis == "mont") {
            int m = conf.get<int>("approx.m");
            WLS<Monomials<vec_t>, GaussianWeight<vec_t>, ScaleToClosest> approx(Monomials<vec_t>::tensorBasis(m), sigmaW);
            return solve_(conf, domain, file, timer, approx);
        }
        throw std::runtime_error("Invalid basis.");
    }

    template<typename vec_t, typename approx_t>
    std::pair<VectorField<double, vec_t::dim>, VectorField<double, vec_t::dim*(vec_t::dim+1)/2>>
    solve_(const XML& conf, DomainDiscretization<vec_t>& d, HDF& out_file, Timer& timer, const approx_t& approx) {

        double a = conf.get<double>("case.a");

        auto interior = d.interior();
        double tol = 1e-7;
        indexes_t down, fixed, rest;
        for (int i : d.boundary()) {
            if (std::abs(d.pos(i, 0) - a) <= tol) down.push_back(i);
            else if (d.pos(i, 0) <= tol) fixed.push_back(i);
            else rest.push_back(i);
        }

        double dx = 1.0/conf.get<double>("num.nx");
        auto gh = d.addGhostNodes(dx, 2);

        int N = d.size();
        prn(N);
        int ss = conf.get<int>("approx.n");
        prn("support")
        d.findSupport(FindClosest(ss));

        double lam = conf.get<double>("case.lam");
        double mu = conf.get<double>("case.mu");

        timer.addCheckPoint("shapes");
        prn("shapes");

        auto storage = d.computeShapes(approx, interior+down+fixed+rest);

        timer.addCheckPoint("matrix");
        prn("matrix");

        const int dim = vec_t::dim;
        SparseMatrix<double, RowMajor> M(dim*N, dim*N);
        M.reserve(Range<int>(dim*N, dim*ss));
        Eigen::VectorXd rhs = Eigen::VectorXd::Zero(dim*N);

        auto op = storage.implicitVectorOperators(M, rhs);

        for (int i : interior) {
            (lam+mu)*op.graddiv(i) + mu*op.lap(i) = 0;
        }

        double F = conf.get<double>("case.F");
        for (int i : down) {
            op.traction(i, lam, mu, d.normal(i)) = {0, 0, -F};
            (lam+mu)*op.graddiv(i, gh[i]) + mu*op.lap(i, gh[i]) = 0;
        }
        for (int i : rest) {
            op.traction(i, lam, mu, d.normal(i)) = 0;
            (lam+mu)*op.graddiv(i, gh[i]) + mu*op.lap(i, gh[i]) = 0;
        }
        for (int i : fixed) {
            op.value(i) = 0;
            (lam+mu)*op.graddiv(i, gh[i]) + mu*op.lap(i, gh[i]) = 0;
        }

        PardisoLU<SparseMatrix<double>> solver;
        SparseMatrix<double> M2(M); M2.makeCompressed();

        timer.addCheckPoint("compute");
        prn("compute");
        solver.compute(M2);
        timer.addCheckPoint("solve");
        prn("solve");

        VectorXd sol = solver.solve(rhs);
        timer.addCheckPoint("postprocess");
        prn("stress computation")
        VectorField<double, dim> disp = VectorField<double, dim>::fromLinear(sol);
        VectorField<double, dim*(dim+1)/2> stress(N);
        auto eop = storage.explicitVectorOperators();
        for (int i : down+fixed+rest+interior) {
            auto grad = eop.grad(disp, i);
            Matrix<double, dim, dim> eps = 0.5*(grad + grad.transpose());
            Matrix<double, dim, dim> s = lam * eps.trace() * Matrix<double, dim, dim>::Identity(dim, dim) + 2*mu*eps;
            int c = 0;
            for (int d1 = 0; d1 < dim; ++d1) {
                for (int d2 = d1; d2 < dim; ++d2) {
                    stress(i, c++) = s(d1, d2);
                }
            }
        }
        timer.addCheckPoint("end");

        out_file.atomic().writeEigen("displ", disp);
        out_file.atomic().writeEigen("stress", stress);

        return {disp,stress};
    }

};

typedef Vec3d vec;

int main(int argc, char* argv[]) {
    if (argc < 2) { print_red("Supply parameter file as the second argument.\n"); return 1; }
    XML conf(argv[1]);
    BeamSolver solver;
    solver.preprocess(conf);
    Timer timer;
    timer.addCheckPoint("domain");

    string output_file = conf.get<string>("meta.file");
    HDF file(output_file, HDF::DESTROY);
    file.writeXML("conf", conf);
    file.close();

    double dx = 1.0/conf.get<double>("num.nx");
    double a = conf.get<double>("case.a");
    double b = conf.get<double>("case.b");
    double c = conf.get<double>("case.c");

    prn("start");
    BoxShape<vec> box(0, {a, b, c});

    std::vector<string> xh = split(conf.get<string>("holes.xh"), ",");
    std::vector<string> yh = split(conf.get<string>("holes.yh"), ",");
    std::vector<string> zh = split(conf.get<string>("holes.zh"), ",");
    std::vector<string> rh = split(conf.get<string>("holes.rh"), ",");
    assert_msg(xh.size() == yh.size(), "Inconsistent data.");
    assert_msg(rh.size() == yh.size(), "Inconsistent data.");

    DomainShape<vec>* shape = new BoxShape<vec>(box);
    if (!xh.empty()) {
        try {
            ShapeDifference<vec> sd = box - BallShape<vec>({stof(xh[0]), stof(yh[0]), stof(zh[0])}, stof(rh[0]));
            for (int i = 0; i < xh.size(); ++i) {
                sd = sd - BallShape<vec>({stof(xh[i]), stof(yh[i]), stof(zh[i])}, stof(rh[i]));
            }
            shape = new ShapeDifference<vec>(sd);
        } catch (std::invalid_argument& e) {
            prn("no holes");
        }
    }

    DomainDiscretization<vec> d = shape->discretizeBoundaryWithStep(dx);

    GeneralFill<vec> fill; fill.seed(conf.get<int>("num.seed"));
    fill(d, dx);

    solver.solve(conf, d, file, timer);

    delete shape;
    file.atomic().writeDomain("domain", d);

    return 0;
}
