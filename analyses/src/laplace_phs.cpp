#include <medusa/Medusa.hpp>
#include "Eigen/SparseCore"
#include "Eigen/IterativeLinearSolvers"
#include "poisson_case.hpp"

using namespace mm;
using namespace std;
using namespace Eigen;

template <int dim>
void analyse(XML& conf) {
    typedef Vec<double, dim> vec;

    auto file = conf.get<std::string>("meta.file");
    HDF hdf(file , HDF::DESTROY);
    hdf.writeXML("conf", conf);
    hdf.close();

    double r1 = conf.get<double>("case.r1");
    double r2 = conf.get<double>("case.r2");
    auto sh = BallShape<vec>(0.0, r2) - BallShape<vec>(0.0, r1);

    vector<string> ns = split(conf.get<string>("case.nxs"), ',');
    for (string s : ns) {
        int nx = std::stoi(s);
        auto fn = [=](const vec& v) { return 1.0/nx; };

        GeneralFill<vec> fill; fill.seed(conf.get<int>("case.seed"));
        hdf.setGroupName(format("/n%04d", nx));
        DomainDiscretization<vec> domain = sh.discretizeBoundaryWithDensity(fn);
        fill(domain, fn);
        hdf.atomic().writeDomain("domain", domain);

        Timer t;
        Solver<vec> solver;
        auto u = solver.solve(domain, conf, hdf, t);
    }
}

int main(int argc, char* argv[]) {
    if (argc < 2) { print_red("Supply parameter file as the second argument.\n"); return 1; }
    XML conf(argv[1]);

    int dim = conf.get<int>("case.dim");
    assert_msg(dim == 1 || dim == 2 || dim == 3, "Dimension %d not supported", dim);
    if (dim == 1) analyse<1>(conf);
    if (dim == 2) analyse<2>(conf);
    if (dim == 3) analyse<3>(conf);

    return 0;
}
