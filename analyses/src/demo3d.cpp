#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include <Eigen/PardisoSupport>

using namespace mm;
using namespace std;

int main(int argc, char* argv[]) {
    typedef Vec3d vec_t;

    HDF domain_file("/mnt/data/ijs/papers/2019_P_Medusa/analyses/domains.h5", HDF::READONLY);
    auto d = DomainDiscretization<vec_t>::load(domain_file, "domain2");
    int N = d.size();
    d.findSupport(FindClosest(45));
    RBFFD<Polyharmonic<double, 3>, vec_t, ScaleToClosest> approx({}, 2);
    auto storage = d.computeShapes<sh::lap|sh::d1>(approx);

    Eigen::SparseMatrix<double, Eigen::RowMajor> M(N, N);
    M.reserve(storage.supportSizes());
    Eigen::VectorXd rhs = Eigen::VectorXd::Zero(N);
    auto op = storage.implicitOperators(M, rhs);
    for (int i : d.interior()) {
        -2.0*op.lap(i) + 8.0*op.grad(i, {2.0, 1.0, -1}) = 1.0;
    }
    for (int i : d.boundary()) {
        op.value(i) = 0.0;
    }
    Eigen::PardisoLU<decltype(M)> solver(M);
    Eigen::VectorXd u = solver.solve(rhs);

    HDF hdf("/mnt/data/ijs/papers/2019_P_Medusa/analyses/demo3d_wip.h5", HDF::DESTROY);
    hdf.writeDomain("domain", d);
    hdf.writeEigen("solution", u);
    return 0;
}
