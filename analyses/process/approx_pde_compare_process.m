prepare

casename = 'laplace_gau_2d_wip';
% casename = 'laplace_gau_noscale_2d_wip';
% casename = 'laplace_gau_noscale_jac_2d_wip';
% casename = 'laplace_mon_2d_wip';
% casename = 'laplace_phs_ord2_wip';
casename = 'laplace_phs_ord2_bad_wip';

file = [datapath casename '.h5'];

info = h5info(file);

a = str2num(h5readatt(file, '/conf', 'case.a'));
anal = @(x, y) sin(a(1)*pi*x).*sin(a(2)*pi*y);


ng = length(info.Groups)-1;
error = zeros(ng, 1);
Ns = zeros(ng, 1);
for i = 1:ng

name = info.Groups(i+1).Name;

pos = h5read(file, [name '/domain/pos']);
types = h5read(file, [name '/domain/types']);
x = pos(:, 1);
y = pos(:, 2);

N = length(x);
Ns(i) = N;

if strcmp(casename, 'laplace_phs_ord2_wip') || strcmp(casename, 'laplace_phs_ord2_bad_wip')
    name = [name '/aug+2'];
end

    
try sol = h5read(file, [name '/sol']); catch, fprintf('Finished early on iter %d\n', i); break; end
asol = anal(x, y);

esol = abs(asol-sol);

e1u = norm(esol, 1) / norm(asol, 1);
e2u = norm(esol, 2) / norm(asol, 2);
einfu = norm(esol, inf) / norm(asol, inf);

error(i) = einfu;

end

save([plotdatapath casename '.mat'], 'error', 'Ns');

% exportf(f3, 'poisson_augmentation_convergence.png', '-m0.75')
