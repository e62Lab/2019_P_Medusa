prepare

casenames = {'laplace_gau_2d',...
            'laplace_gau_noscale_2d',...
            'laplace_gau_noscale_jac_2d',...
            'laplace_mon_2d',...
            'laplace_phs_ord2'}; % 'laplace_phs_ord2_bad'

setfig('b3'); leg = {};

nc = length(casenames);
for cidx = 1:nc

casename = [casenames{cidx} '_wip'];
    
file = [datapath casename '.h5'];

info = h5info(file);

ng = length(info.Groups)-1;
error = zeros(ng, 1);
Ns = zeros(ng, 1);
for i = 1:ng

name = info.Groups(i+1).Name;

pos = h5read(file, [name '/domain/pos']);
types = h5read(file, [name '/domain/types']);
I = types > 0;
x = pos(I, 1);
y = pos(I, 2);

N = length(x);
Ns(i) = N;

if strcmp(casename, 'laplace_phs_ord2_wip') || strcmp(casename, 'laplace_phs_ord2_bad_wip')
    name = [name '/aug+2'];
end

    
try sol = h5read(file, [name '/lap']); catch, fprintf('Finished early on iter %d\n', i); break; end

sol = sol(I);
asol = -2*pi*pi*sin(pi*x).*sin(pi*y);

esol = abs(asol-sol);

e1u = norm(esol, 1) / norm(asol, 1);
e2u = norm(esol, 2) / norm(asol, 2);
einfu = norm(esol, inf) / norm(asol, inf);

error(i) = einfu;

end

% setfig('b1');
% plot(Ns, error, 'o-')
% ll
% 
% setfig('b2');
% scatter(x, y, 15, sol, 'filled')
% axis equal
% colorbar
% 
% setfig('b4');
% scatter(x, y, 15, asol, 'filled')
% colorbar
% axis equal

save([plotdatapath casename '.mat'], 'error', 'Ns');
plot(Ns, error, 'o-'); leg{end+1} = replace(casename, '_', '-');

end

legend(leg);
ll
ylim([1e-6 1e-1])