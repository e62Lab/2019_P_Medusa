prepare

casename = 'demo2d';
file = [datapath casename '.h5'];

pos = h5read(file, '/domain/pos');
sol = h5read(file, '/solution');

x = pos(:, 1);
y = pos(:, 2);

save([plotdatapath casename '.mat'], 'x', 'y', 'sol');