prepare

casename = 'beam3d';
% casename = 'beam3d_holes';

datafile = [datapath casename '.h5'];

info = h5info(datafile);

E = h5readatt(datafile, '/conf', 'case.E');
nu = h5readatt(datafile, '/conf', 'case.nu');

name = '/';

pos = h5read(datafile, [name '/domain/pos']);
N = length(pos);
x = pos(:, 1);
y = pos(:, 2);
z = pos(:, 3);

types = h5read(datafile, [name '/domain/types']);
ghost = find(types == 2);

name = '/aug+2';

sol = h5read(datafile, [name '/displ']);
sol(ghost, :) = nan;

sol = sol(1:N, :);
u = sol(:, 1);
v = sol(:, 2);
w = sol(:, 3);
dnorm = sum(sol.^2, 2);

stress = h5read(datafile, [name, '/stress']);
stress(ghost, :) = nan;
stress = stress(1:N, :);
sxx = stress(:, 1);
sxy = stress(:, 2);
sxz = stress(:, 3);
syy = stress(:, 4);
syz = stress(:, 5);
szz = stress(:, 6);
sv = von_mises(sxx, syy, szz, sxy, sxz, syz);

save([plotdatapath casename '.mat'], 'x', 'y', 'z', 'u', 'v', 'w', 'sv', 'N', 'E', 'nu');
