
fn=[working_dir,case_name,'.h5'];
info = hdf5info(fn);
no_steps=size(info.GroupHierarchy.Groups,2);
if(~exist('ii','var')); ii=no_steps - 1; end

% support = h5read(fn,'/step0/support');
types = h5read(fn,'/step0/types');
pos = h5read(fn,'/step0/positions');
%normals = h5read(fn,'/step0/normals');
%boundary_map = h5read(fn,'/step0/boundary_map');
%skin = h5read(fn,'/step0/skin');

%air_lam = h5readatt(fn,'/step0','air_lam');
%air_c_p = h5readatt(fn,'/step0','air_c_p');
%air_rho = h5readatt(fn,'/step0','air_rho');
%air_mu = h5readatt(fn,'/step0','air_mu');
%air_beta = h5readatt(fn,'/step0','air_beta');
%skin = h5read(fn,'/step0/skin');
%dt = h5readatt(fn,'/step0','dt');

% actual load

step=['/step',num2str(ii),'/'];
for var=regV eval([cell2mat(var),'=','h5read(''',fn,''',''',[step,cell2mat(var)],''');']);end
for var=atts eval([cell2mat(var),'=','h5readatt(''',fn,''',''',step,''',''',cell2mat(var),''');']);end

%debug load.
% addpath(working_dir);
% eval(case_name);
curr_step = ii;

idx_d = types == 9;
pos(idx_d, :) = [];
T(idx_d) = [];
v(idx_d, :) = [];
