function mainPlot()

% tic    
% %%SETUP
tic
    close all
    clc;
    clear all;
    cmap = open('cmap.mat');
    
    fontname = 'Times';
    set(0,'defaultaxesfontname',fontname);
    set(0,'defaulttextfontname',fontname);
    
    fontsize = 13;
    set(0,'defaultaxesfontsize',fontsize);
    set(0,'defaulttextfontsize',fontsize);
    
    working_dir='./';
    addpath(working_dir);

    case_name='thermofluid_3d_thermofluid_3d_holes';
%     case_name='thermofluid_3d_thermofluid_3d';
    
    
%% PLOT DEFINITIONS
    plot_pos        = 1;
        plot_IT     = 1;
        scale       = 5;
    
    contour_resolution = [100,100]; 
    % REGISTERED VARIABLES -- variables to load from HDF5
    regV={'T', 'v'};
    atts = {'time'};
    % matrix
%     eval(case_name); 
    thermofluid_load_data;
    
    if 0
        M = spconvert(h5read(fn, '/M2')');
%         rhs  = h5read(fn, '/rhs');
        disp(['cond no of a system: ', num2str(condest(M),'%e')]);
        spy(M);
    end
    %%
    %FIGURE CONFIG
    fig=createFig('ps', 'm2 1x2 p1','scale',0);hold on;    
    axis('equal');

    %%REGISTER PLOTS
    rP={};iip=0;%registered plots - system variable thaT have to be filled for each plot
    rS={};iis=0;%registered scattered plots - system variable thaT have to be filled for each plot
    rS3={};iis3=0;%registered scattered plots - system variable thaT have to be filled for each plot
    rV={};iiv=0;%registered vector plots
    rV3={};iiv3=0;%registered 3D vector plots
      
    if plot_pos && exist('pos') && ~exist('pp') 
        if 0 %contour plot - only one allowed
               rC =  {'pos(:,1)', 'pos(:,2)', 'T' }; 
               [~, pc] = scontour(eval(cell2mat(rC(1))), eval(cell2mat(rC(2))), eval(cell2mat(rC(3))), contour_resolution(1), contour_resolution(1)...
                   , 'linestyle', 'none', 'fill', 'on');
               colormap(cmap.Temp)
               colorbar;
        end
        if 0 %2D Nodes plotting
            plot(pos(:,1),pos(:,2),'ko'...
                ,'markersize', 2,'linestyle','none'); 
        end
        if 1 % registered 3d scatter plotting
            iis3 = iis3 + 1;
            rS3=[rS3;{'pos(:,1)','pos(:,2)','pos(:, 3)','18*ones(size(T))','T'}];
            xp=scatter3(eval(cell2mat(rS3(1))),eval(cell2mat(rS3(2))),eval(cell2mat(rS3(3))),eval(cell2mat(rS3(4))), eval(cell2mat(rS3(5))));   
            ps3(iis3)=xp;
            view([-30, 15])
        end        
        if 0 %register scatter plot
            iis=iis+1;
            if ~exist(scattvar,'var') eval([scattvar,'=ones(1, length(v));']);end
            rS=[rS;{'pos(1,types>0)','pos(2,types>0)',[scattvar,'(types>0)']}];
            xp=scatter(eval(cell2mat(rS(end,1))),eval(cell2mat(rS(end,2))),[8],eval(cell2mat(rS(end,3))),'marker','.');   
            ps(iis)=xp; 
        end   
        if 0 %register quiver plot 
%             idx = types == 4 | types == -4 | types == -3;
            iiv=iiv+1;
            rV=[rV;{'pos(:,1)','pos(:,2)','v(:,1)','v(:,2)'}];
            pv(iiv)=quiver(eval(cell2mat(rV(end,1))),...
            eval(cell2mat(rV(end,2))),eval(cell2mat(rV(end,3))),...
            eval(cell2mat(rV(end,4))),scale,'color','k');   
        end
        if 1 %register 3D quiver plot
            iiv3=iiv3+1;
            rV3=[rV3;{'pos(:,1)','pos(:,2)','pos(:,3)',...
                'v(:,1)','v(:,2)','v(:,3)'}];
            pv3(iiv3)=quiver3(eval(cell2mat(rV3(1))),...
            eval(cell2mat(rV3(2))),eval(cell2mat(rV3(3))),...
            eval(cell2mat(rV3(4))),eval(cell2mat(rV3(5))),...
            eval(cell2mat(rV3(6))),scale,'color','k');
        end  
    end

    if plot_IT
        set(fig,'KeyPressFcn',@keyDownListener);
    end

    %% skin profile
    if 0
        fig=createFig('ps', 'm2 1x2 p2','scale',0);hold on; 
        plot(atan2(pos(2,skin+1),pos(1,skin+1)) + pi, T(skin+1),'+');     
        set(gca, 'XTick', 0:pi/4:2*pi)
        set(gca, 'XTickLabel',{'0','\pi/4','\pi/2','3\pi/4','\pi','5/4\pi','3/2\pi','7\pi/4','2pi'})
        ylabel('T[C]')
    end
    %% % H-Cross section plot DVD / LD
    if 0
        fig=createFig('ps', 'm2 1x2 p2','scale',0);hold on;    
        x = v(:,2);
        [x1, y1, a1] = interpolateToMeshGrid(pos(:,1), pos(:,2), x, 400, 400);
        plot(x1(200,:),a1(200,:),'k.', 'displayName', 'T' );
        xlabel('x');
        ylabel('v');   
    end
    %% % V-profile plot - CH
    if 0
        fig=createFig('ps', 'm2 1x2 p2','scale',0);hold on;    
        x = v(:,1);
        [x1, y1, a1] = interpolateToMeshGrid(pos(:,1), pos(:,2), x, 400, 400);
        cross_section = 7;
        plot(y1(:,find(x1(1,:)>=cross_section,1)),a1(:, find(x1(1,:)>=cross_section,1)),'k.', 'displayName', 'T' );
        xlabel('y');
        ylabel('u');
        title(num2str(x1(1,find(x1(1,:)>=cross_section,1))));
    end
    %%
    if 1 % 3D cross-section plot
       
        i_num = 50;
        [xp,yp,zp,vT] = interpolateToMeshGrid3D(pos(:,1),pos(:,2),pos(:,3), T, i_num, i_num, i_num);
        [xp,yp,zp,vu] = interpolateToMeshGrid3D(pos(:,1),pos(:,2),pos(:,3), v(:,1), i_num, i_num, i_num);
        [xp,yp,zp,vv] = interpolateToMeshGrid3D(pos(:,1),pos(:,2),pos(:,3), v(:,2), i_num, i_num, i_num);
        [xp,yp,zp,vw] = interpolateToMeshGrid3D(pos(:,1),pos(:,2),pos(:,3), v(:,3), i_num, i_num, i_num);
        
        if 1 %slice plot
        fig1=createFig('ps', 'm2 2x2 p3','scale',1);hold on; 
        xslices = [0.5];
        yslices = [0.5];
        zslices = [0.5];
        h = slice(xp,yp,zp,vT,xslices,yslices, zslices);
        set(h,'EdgeColor','none','FaceColor','interp','FaceAlpha','interp');
        alpha('color')
        alphamap('rampdown')
        alphamap('increase',0.2)
        colorbar;
        view([-30, 30]);
        colormap(cmap.Temp)
        end
        %xz - plane
        fig1=createFig('ps', 'm2 2x2 p4','scale',1);hold on; 
        contour(squeeze(xp(floor(i_num/2),:,:)),squeeze(zp(floor(i_num/2),:,:)),...
            squeeze(vT(floor(i_num/2),:,:)),'fill','on');
        quiver(squeeze(xp(floor(i_num/2),:,:)),squeeze(zp(floor(i_num/2),:,:)),squeeze(vu(floor(i_num/2),:,:)),squeeze(vw(floor(i_num/2),:,:)),scale)
        xlabel('x');ylabel('z');
        colormap(cmap.Temp)
        %% vel cross-section
        fig1=createFig('ps', 'm2 2x2 p3','scale',1);hold on; 
%%
       plot(squeeze(xp(floor(i_num/2),:,floor(i_num/2))),squeeze(vw(floor(i_num/2),:,floor(i_num/2)))); 
       xlabel('x');ylabel('w(x,0.5,0.5)');
%%
    end
    toc
    evalin('base','clear')
    W = who; 
    putvar(W{:});

end

%%IT TOOLS functions
function keyDownListener(~, event)
    switch event.Key
    case 'leftarrow'
        evalin('base',...
        'curr_step=curr_step-1;if (curr_step < 0) curr_step=no_steps-1; end;');
    case 'rightarrow'
        evalin('base',...
        'curr_step=curr_step+1;if (curr_step == no_steps) curr_step=0; end');
    end
    
    %reload registered data
    evalin('base', 'ii=curr_step');
    evalin('base', 'load_data');
    %step=['/step',num2str(evalin('base','curr_step')),'/'];
    %fn=evalin('base','fn');
    %for var=evalin('base','regV') evalin('base',[cell2mat(var),'=','h5read(''',fn,''',''',[step,cell2mat(var)],''');']);end
    
   %update registered ordinary plots 
    if 0
        rP=evalin('base','rP'); 
        for i=1:size(rP,1)
          set(evalin('base',['pp(',num2str(i),')']), 'xdata',evalin('base',['eval(cell2mat(rP(',num2str(i),',1)))']));  
          set(evalin('base',['pp(',num2str(i),')']), 'ydata',evalin('base',['eval(cell2mat(rP(',num2str(i),',2)))'])); 
        end
    end
    
    %update registered scatter plots 
    rS=evalin('base','rS'); 
    for i=1:size(rS,1)
      set(evalin('base',['ps(',num2str(i),')']), 'xdata',evalin('base',['eval(cell2mat(rS(',num2str(i),',1)))']));  
      set(evalin('base',['ps(',num2str(i),')']), 'ydata',evalin('base',['eval(cell2mat(rS(',num2str(i),',2)))']));
      set(evalin('base',['ps(',num2str(i),')']), 'cdata',evalin('base',['eval(cell2mat(rS(',num2str(i),',3)))']));  
    end  
    %update registered scatter3 plots
    rS=evalin('base','rS3'); 
    for i=1:size(rS,1)
      set(evalin('base',['ps3(',num2str(i),')']),...
          'cdata',evalin('base',evalin('base','cell2mat(rS3(5))')));  
    end
    %update registered vector plots 
    rV=evalin('base','rV'); 
    for i=1:size(rV,1)
      set(evalin('base',['pv(',num2str(i),')']), 'xdata',evalin('base',['eval(cell2mat(rV(',num2str(i),',1)))']));  
      set(evalin('base',['pv(',num2str(i),')']), 'ydata',evalin('base',['eval(cell2mat(rV(',num2str(i),',2)))'])); 
      set(evalin('base',['pv(',num2str(i),')']), 'udata',evalin('base',['eval(cell2mat(rV(',num2str(i),',3)))']));  
      set(evalin('base',['pv(',num2str(i),')']), 'vdata',evalin('base',['eval(cell2mat(rV(',num2str(i),',4)))']));  
    end
    %update registered 3D vector plots 
    rV3=evalin('base','rV3'); 
    for i=1:size(rV3,1)
      set(evalin('base',['pv3(',num2str(i),')']), 'udata',evalin('base','eval(cell2mat(rV3(4)))'));  
      set(evalin('base',['pv3(',num2str(i),')']), 'vdata',evalin('base','eval(cell2mat(rV3(5)))'));
      set(evalin('base',['pv3(',num2str(i),')']), 'wdata',evalin('base','eval(cell2mat(rV3(6)))'));  
    end
    %update contour plot
    if(evalin('base', 'exist(''pc'',''var'')'))
        rC = evalin('base','rC');
        [~, ~, zz] = ...
        evalin('base', ['interpolateToMeshGrid('...
            ,cell2mat(rC(1)), ',', cell2mat(rC(2)),',', cell2mat(rC(3)),...
            ',contour_resolution(1), contour_resolution(2), ''interpolation'', ''cubic'', ''out'', ''mesh'')']);  
        x = evalin('base','pc'); 
        x.ZData = zz;
    end
    evalin('base','title([''t='',num2str(time,''%2.2f'')])')
    disp([num2str(evalin('base','curr_step'))]);
end

function [min_dist, min_p1, min_p2, II] = closest_pair(x, y)
    if isrow(x), x = x'; end
    if isrow(y), y = y'; end
    KDT = KDTreeSearcher([x, y]);
    idx = knnsearch(KDT, [x, y], 'K', 2);
    dist = sqrt((x - x(idx(:, 2))).^2 + (y - y(idx(:, 2))).^2);
    [min_dist, I] = min(dist);
    min_p1 = I;
    min_p2 = idx(I, 2);
    II = dist<0.00001;
end

