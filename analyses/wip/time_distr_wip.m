prepare

casename = 'time_distr_wip';
file = [datapath casename '.h5'];

info = h5info(file);


nxs = str2double(strsplit(h5readatt(file, '/conf', 'case.nxs'), ','));
ths = 1;

nsize = length(nxs);
tsize = length(ths);
Ns = zeros(nsize, 1);
times = zeros(nsize, tsize, 7);
total = zeros(nsize, tsize);
err = zeros(nsize, tsize, 3);
l1 = 1; l2 = 2; linf = 3;

timesleg = {'domain', 'support', 'weights', 'assemble', 'decompose', 'solve'};

for i = 1:nsize
for j = 1:tsize
    try
    gname = info.Groups(i+1).Name;
    catch, break, end
    
    Ns(i) = h5readatt(file, gname, 'N');
    domain = h5readatt(file, [gname '/times'], 'domain-support');
    support = h5readatt(file, [gname '/times'], 'support-approx');
    approx = h5readatt(file, [gname '/times'], 'approx-matrix');
    matrix = h5readatt(file, [gname '/times'], 'matrix-compute');
    compute = h5readatt(file, [gname '/times'], 'compute-solve');
    solve = h5readatt(file, [gname '/times'], 'solve-postprocess');
    post = h5readatt(file, [gname '/times'], 'postprocess-end');
   
    times(i, j, :) = [domain support approx matrix compute solve post];
    total(i, j) = h5readatt(file, [gname '/times'], 'total');
    err(i, j, linf) = h5readatt(file, gname, 'err_inf');
    err(i, j, l1) = h5readatt(file, gname, 'err_1')/Ns(i);
    err(i, j, l2) = h5readatt(file, gname, 'err_2')/sqrt(Ns(i));
end
end

setfig('b1');
threadidx = 1;
area(Ns, squeeze(times(:, threadidx, :)))
legend(timesleg, 'Location', 'NW');
set(gca, 'xscale', 'log')
title(sprintf('Time distribution with %d threads.', ths(threadidx)));
% set(gca, 'yscale', 'log')
xlabel('$N$')
ylabel('time [s]')

f2 = setfig('b2', [700 400]);
leg = cell(size(timesleg));
for j = 1:6
    k = polyfit(log(Ns), log(times(:, 1, j)), 1);
    plot(Ns, times(:, 1, j), 'o-');
    leg{j} = [timesleg{j} sprintf(' $k = %.2f$', k(1))];    
end
plot(Ns, total(:, 1), '-xk'); leg{end+1} = 'total time';

ll
ylabel('execution time [s]')
xlim([3e3 1.2e6])
legend(leg, 'Location', 'EastOutside');

f4 = setfig('b4', [700 400]);
for j = 1:6
    plot(Ns, 100*times(:, 1, j) ./ total(:, 1), 'o-');
end
% title('Time percentage distribution solving Poisson''s equation in 3D.');
xlabel('$N$')
xlim([3e3 1.2e6])
ylim([0 55])
ylabel('\% of total time')
set(gca, 'xscale', 'log')
legend(timesleg, 'Location', 'EastOutside');

setfig('b3');
errleg = {};
errleg{l1} = 'L1'; errleg{l2} = 'L2'; errleg{linf} = 'Linf';
for e = [l1, l2, linf]
    k = polyfit(log(Ns), log(err(:, 1, e)), 1);
    plot(Ns, err(:, 1, e), 'o-'); errleg{e} = [errleg{e} sprintf(' $k = %.2f$', k(1))];
end
legend(errleg);
ll

exportf(f2, [imagepath 'time_distr.pdf']);
exportf(f4, [imagepath 'time_distr_ratio.pdf']);