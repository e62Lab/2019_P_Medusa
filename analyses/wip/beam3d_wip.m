prepare

casename = 'beam3d_wip';
% casename = 'beam3d_holes_wip';
datafile = [datapath casename '.h5'];

info = h5info(datafile);

E = h5readatt(datafile, '/conf', 'case.E');
nu = h5readatt(datafile, '/conf', 'case.nu');

name = '/';

pos = h5read(datafile, [name '/domain/pos']);
N = length(pos);
x = pos(:, 1);
y = pos(:, 2);
z = pos(:, 3);

types = h5read(datafile, [name '/domain/types']);
ghost = find(types == 2);

name = '/aug+2';

sol = h5read(datafile, [name '/displ']);
sol(ghost, :) = nan;

sol = sol(1:N, :);
u = sol(:, 1);
v = sol(:, 2);
w = sol(:, 3);
dnorm = sum(sol.^2, 2);

stress = h5read(datafile, [name, '/stress']);
stress(ghost, :) = nan;
stress = stress(1:N, :);
sxx = stress(:, 1);
sxy = stress(:, 2);
sxz = stress(:, 3);
syy = stress(:, 4);
syz = stress(:, 5);
szz = stress(:, 6);
sv = von_mises(sxx, syy, szz, sxy, sxz, syz);

setfig('b1');  view(3)
scatter3(x, y, z, 5, dnorm, 'filled');
% quiver3(x, y, z, u, v, w, 1);
daspect([1 1 1])
colorbar

setfig('b3'); view(3)
f = 5e4;
scatter3(x+f*u, y+f*v, z+f*w, 5, sv, 'filled');
daspect([1 1 1])
colorbar
colormap('jet')
ylim([0 5])
xlim([0 16])
zlim([-3 2])
caxis([0 5e4])

if 0

[X, Y, Z] = meshgrid(deal(linspace(-1, 1, 100)));

setfig('b4'); view(3)
title('Displacement norm $\|\vec{u}\|$')
V = griddata(x, y, z, dnorm, X, Y, Z);

contourslice(X, Y, Z, V, -0.5, 0.5, [], 20);
axis equal
colorbar
colormap('jet')


setfig('b3'); view(3)
title('Von Mises stress $\sigma_v$')

I = find(y > 0);
% scatter3(x(I), y(I), z(I), 15, sv(I))

sv(sv > 0.2) = 0.2;

V = griddata(x, y, z, sv, X, Y, Z);
contourslice(X, Y, Z, V, -0.5, 0.5, [], 20)
axis equal
colorbar
colormap('jet')
caxis([0, 0.2])

end