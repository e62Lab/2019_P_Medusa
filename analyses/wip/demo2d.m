prepare

file = [datapath 'demo2d_wip.h5'];

pos = h5read(file, '/domain/pos');
types = h5read(file, '/domain/types');
sol = h5read(file, '/solution');

int = h5read(file, '/int');
bnd = h5read(file, '/bnd');
crc = h5read(file, '/crc');
types = -9;
types(int+1) = 2;
types(bnd+1) = -2;
types(crc+1) = 1;

x = pos(:, 1);
y = pos(:, 2);

f1 = setfig('b1', [600 400]);
scatter(x, y, 15, sol, 'filled');
axis equal
colorbar
colormap jet
xlim([-inf inf])
ylim([-inf, inf])