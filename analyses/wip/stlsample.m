close all
format compact

file = 'test.h5';

pos = h5read(file, '/domain/pos');
x = pos(:, 1);
y = pos(:, 2);
z = pos(:, 3);

length(x)

setfig('b1');
plot_domain(file, '/domain');

setfig('b3');
view(3);
scatter3(x, y, z, 5, 'ok', 'filled');
daspect([1 1 1])


[d, p1, p2] = closest_pair(pos);

d
pos(p1, :)
pos(p2, :)

[idx, d] = knnsearch(pos, pos, 'K', 7);

setfig('b2');
histogram(d(:, 2:6), 100)
set(gca, 'xscale', 'log')