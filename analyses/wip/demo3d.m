prepare

casename = 'demo3d';
file = [datapath casename '.h5'];

pos = h5read(file, '/domain/pos');
sol = h5read(file, '/solution');

x = pos(:, 1);
y = pos(:, 2);
z = pos(:, 3);

f1 = setfig('b1', [700 500]); view([ -27.1000   34.8000])
daspect([1 1 1])
scatter3(x, y, z, 5, sol, 'filled');
zlim([-2 0])
xlim([-3, 15])
ylim([-3, 9])
caxis([0 inf])
cb = colorbar;
set(cb,'position',[ 0.8500    0.6480    0.0529    0.1900])
colormap jet

exportf(f1, [imagepath casename '.png'])