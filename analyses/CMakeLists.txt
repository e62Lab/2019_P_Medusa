project(medusa_paper)
cmake_minimum_required(VERSION 2.8.12)

# Change to your path to medusa:
include_directories(${CMAKE_SOURCE_DIR}/../../../medusa/include/)
link_directories(${CMAKE_SOURCE_DIR}/../../../medusa/bin/)
add_subdirectory(${CMAKE_SOURCE_DIR}/../../../medusa/ medusa)

# For pardiso solver.
include_directories(/opt/intel/compilers_and_libraries/linux/mkl/include)
link_directories(/opt/intel/compilers_and_libraries/linux/mkl/lib/intel64)
link_directories(/opt/intel/compilers_and_libraries/linux/lib/intel64)

function(register name)
    add_executable(${name} src/${name}.cpp)
    target_link_libraries(${name} medusa_standalone hdf5)
    target_compile_options(${name} PRIVATE -fopenmp -std=c++11 -O3)
    # For pardiso solver.
    target_compile_definitions(${name} PUBLIC EIGEN_USE_MKL_VML MKL_LP64)  # NDEBUG
    target_link_libraries(${name} mkl_intel_lp64 mkl_intel_thread mkl_core pthread iomp5)
endfunction(register)

register(approx)
register(approx_sample)
register(laplace_phs)
register(domain)
register(demo2d)
register(demo3d)
register(el2dbeam)
register(el3dbeam)

add_executable(thermofluid_2d src/thermofluid_2d.cpp)
target_compile_options(thermofluid_2d PRIVATE -std=c++11 -O3 -fopenmp)
target_compile_definitions(thermofluid_2d PRIVATE NDEBUG)
target_link_libraries(thermofluid_2d medusa gomp)

add_executable(thermofluid_3d src/thermofluid_3d.cpp)
target_compile_options(thermofluid_3d PRIVATE -std=c++11 -O3 -fopenmp)
target_compile_definitions(thermofluid_3d PRIVATE NDEBUG)
target_link_libraries(thermofluid_3d medusa gomp)

add_executable(time2d src/time2d.cpp)
target_compile_options(time2d PRIVATE -std=c++11 -O3)
target_compile_definitions(time2d PRIVATE NDEBUG)
target_link_libraries(time2d medusa)

add_executable(time3d src/time3d.cpp)
target_compile_options(time3d PRIVATE -std=c++11 -O3)
target_compile_definitions(time3d PRIVATE NDEBUG)
target_link_libraries(time3d medusa)

add_executable(time_distr src/time_distr.cpp)
target_compile_options(time_distr PRIVATE -std=c++11 -O3)
target_compile_definitions(time_distr PRIVATE NDEBUG)
target_link_libraries(time_distr medusa)
