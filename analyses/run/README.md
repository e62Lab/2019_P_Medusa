# Running benchmarks

To run benchmarks, make sure you first compiled the executables `time2d` and `time3d`, and the you
have FreeFem++ installed. Then execute the following from the `run/` folder
```
mkdir ../wip  # folder for results
python run2d.py medusa
python run2d.py ff
python run3d.py medusa
python run3d.py ff
```

The plottingprocess is the same as explained in the `analyses/` folder.
