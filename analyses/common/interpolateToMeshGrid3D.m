function [xo, yo, zo, fo] = interpolateToMeshGrid3D(x, y, z, f, nx, ny, nz, varargin)
%interpolateToMeshGrid interpolate [x,y,f] -->
% [xi,yi,fi] - where xi = min(x):max-min/nx:max(x)
%returns interpolated field fi and its meshgrid reshapes fim, xim, yim
%example:

% xx=[];
% yy=[];
% zz=[];
% ff=[];
% 
% for x=0:0.1:1
% for y=1:0.1:2
% for z=1:0.1:5
% 
%   ff=[ff,sin(3*x.*2*y)+3*z];
%   xx=[xx,x];
%   yy=[yy,y];
%   zz=[zz,z];
% end
% end
% end
% 
% [xi,yi,zi,fi]=interpolateToMeshGrid3D(xx,yy,zz,ff,51,51,51,'interpolation','linear','out','vec');
% [xim,yim,zim,fim]=interpolateToMeshGrid3D(xx,yy,zz,ff,51,51,51,'interpolation','linear','out','mesh');


p = inputParser;
addOptional(p, 'interpolation', 'linear'); %interpolation type -- linear, cubic ...
addOptional(p, 'out', 'mesh'); %what to return, meshgrids (mesh) or vectors (vec)

%%parameters parsing
try
    parse(p, varargin{:});
catch err
    fprintf(2, 'PARSER ERROR');
    disp('valid arguments >>')
    disp(p.Parameters);
    disp('error msg:')
    rethrow(err);
end

for (param = p.Parameters) eval([cell2mat(param), '=p.Results.', cell2mat(param), ';']); end

xscale = [min(x), max(x)];
yscale = [min(y), max(y)];
zscale = [min(z), max(z)];
xi = linspace(xscale(1), xscale(2), nx);
yi = linspace(yscale(1), yscale(2), ny);
zi = linspace(zscale(1), zscale(2), nz);

[xim, yim, zim] = meshgrid(xi, yi, zi);

fim = griddata(double(x), double(y), double(z), double(f), double(xim), double(yim), double(zim), interpolation);
fi = reshape(fim, nx*ny*nz, 1);
xi = reshape(xim, nx*ny*nz, 1);
yi = reshape(yim, nx*ny*nz, 1);
zi = reshape(zim, nx*ny*nz, 1);
if strcmp(out, 'mesh')
    xo = xim;
    yo = yim;
    zo = zim;
    fo = fim;
else
    xo = xi;
    yo = yi;
    zo = zi;
    fo = fi;
end
