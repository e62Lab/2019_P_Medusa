% sets log log plot
set(gca, 'xscale', 'log');
set(gca, 'yscale', 'log');
xlabel('$N$')
ylabel('error')

% vim: set ft=matlab:
