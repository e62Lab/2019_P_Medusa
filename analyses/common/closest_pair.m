function [min_dist, min_p1, min_p2] = closest_pair(pos)
% Returns closest pair between two sets of points, given with x and y coordinates.

[idx, d] = knnsearch(pos, pos, 'K', 2);
[min_dist, I] = min(d(:, 2));
min_p1 = I;
min_p2 = idx(I, 2);

end
