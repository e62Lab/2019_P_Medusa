function newval = sheppard(pos, val, newpos, R, p)
% Performs Modified Sheppard's interpolation.
%
% Interpolates function given by (pos, val) to
% new positions newpos.
%
% pos = N x d matrix of d-dimensional points
% val = N x 1 matrix of scalar values
% newpos = M x d matrix of d-dimensional points
% R = radius of influence
% p = power (2 by default)
%
% Time complexity: O(k M 2^d \log N), k = max number of points in radius R

if nargin < 5, p = 2; end
newval = zeros(size(newpos, 1), 1);
[I, d] = rangesearch(pos, newpos, R);

for i = 1:length(newval)
    w = ((R - d{i})./(R*d{i})).^2;
    newval(i) = w*val(I{i})/sum(w);
end

end
