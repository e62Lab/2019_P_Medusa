function nv = sinterp3(x, y, z, v, nx, ny, nz, Method, ExtrapolationMethod)
% Interpolate scattered data outside convex hull of x, y as well.
    assert(nargin >= 7);
    if nargin < 9, ExtrapolationMethod = 'linear'; end
    if nargin < 8, Method = 'linear'; end
    if ~iscolumn(x), x = x'; end
    if ~iscolumn(y), y = y'; end
    if ~iscolumn(v), v = v'; end
    F = scatteredInterpolant(x, y, z, v, Method, ExtrapolationMethod);
    nv = F(nx, ny, nz);
end
