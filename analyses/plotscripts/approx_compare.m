prepare
casenames = {'laplace_gau_2d', 'laplace_gau_noscale_2d',...
             'laplace_gau_noscale_jac_2d', 'laplace_mon_2d',...
             'laplace_phs_ord2'};
nc = length(casenames);

f1 = setfig('b1', [600 400]);
ll;

type = {'-o', '-x', '-*', '-+', '-^', '-d'};
C = linespecer(nc);
C = zeros(size(C));


set(0, 'DefaultLineLineWidth', 0.8);
set(0, 'DefaultLineMarkerSize', 4);

for i = 1:nc
    D = load([plotdatapath casenames{i} '.mat']);
    plot(D.Ns, D.error, type{i}, 'Color', C(i, :)); 
end
ls = cell(nc, 1);
for i = 1:nc, ls{i} = sprintf('Case (%d)', i); end
legend(ls, 'Location', 'SW');
% xlabel('$\sqrt{N}$');
ylabel('$\ell^\infty$ error');
% xticks([10:10:80 100 150 200:100:1000]);
xticks([1.5e3 3e3 1e4 3e4 1e5 3e5]);
xlim([1.4e3 3e5])
ylim([1e-6 1e-2])

% exportf(f1, [imagepath 'approx_behaviour.pdf'])