prepare

file = [datapath 'domains.h5'];

pos = h5read(file, '/domain2/pos');
x = pos(:, 1);
y = pos(:, 2);
z = pos(:, 3);
length(x)

types = h5read(file, '/domain2/types');
I = types > 0;
B = types < 0;

supp = h5read(file, '/domain2/supp')+1;

f1 = setfig('b1', [700 500]); view([ -27.1000   34.8000])
daspect([1 1 1])
scatter3(x(I), y(I), z(I), 5, 'ok', 'filled');
scatter3(x(B), y(B), z(B), 20, 'xk');

bmap = h5read(file, '/domain2/bmap')+1;
normals = h5read(file, '/domain2/normals');
bnd = bmap(B);
quiver3(x(B), y(B), z(B), normals(bnd, 1), normals(bnd, 2), normals(bnd, 3), 1, 'Color', 0.5*[1 1 1]);

% f = 1.1;
xlim([-3, 15])
ylim([-3, 9])
zlim([-2.5, 2.5])

title(sprintf('$N = %d$', length(x)));

% set(lgd, 'Position', [0.1652    0.7552    0.2525    0.2310]);

% exportf(f1, [imagepath 'domain2.png'], '-m2');