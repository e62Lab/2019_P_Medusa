prepare
casename = 'demo2d';
load([plotdatapath casename '.mat'])

pos = [x y];

h = 0.01;
[X, Y] = meshgrid(-2:h:2.5, -2:h:2);
newpos = [X(:), Y(:)];
V = sheppard(pos, sol, newpos, 9);

X = reshape(newpos(:, 1), size(X));
Y = reshape(newpos(:, 2), size(X));
V = reshape(V, size(X));

poly = [-2, 0; -1.15, 0; -0.366, -1.366; 2.232, 0.134; 1.232, 1.866; 0.616, 0.933; -1, 0.475; -1, 1; -2, 1];
poly(end+1, :) = poly(1, :);
c = [0.965 0.325];
r = 0.3;
C = (X-c(1)).^2 + (Y-c(2)).^2 < r^2;
V(C) = nan;
P = ~inpolygon(X, Y, poly(:, 1), poly(:, 2));
V(P) = nan;

f1 = setfig('b1', [600 400]);
scatter(x, y, 5, sol, 'filled');
contour(X, Y, V, 50);
plot(poly(:, 1), poly(:, 2), '-k');
phi = linspace(0, 2*pi, 100);
plot(c(1) + r*cos(phi), c(2) + r*sin(phi), '-k');
axis equal
colorbar
colormap jet
xlim([-2.2 2.5])
ylim([-1.5 2])

% exportf(f1, [imagepath casename '.pdf']);