prepare
%     cmap = open('cmap.mat');
Resolution = 300;
fontname = 'Times';
set(0,'defaultaxesfontname',fontname);
set(0,'defaulttextfontname',fontname);

fontsize = 16;
set(0,'defaultaxesfontsize',fontsize);
set(0,'defaulttextfontsize',fontsize);

%% 2D
if 0
load([plotdatapath 'DVD_IMP_PHS_Ra8']);
fig1=createFig('ps', 'm1 4x2 p4','scale',0.9);hold on;
contour(x1, y1, TT,'fill','on','levelstep',0.02);
colormap('jet');
cH = colorbar;
set(cH, 'YTick', [-1,1]);
cH.TickLabelInterpreter = 'latex';
cH.TickLabels{1} = '$T_{cold}$';
cH.TickLabels{end} = '$T_{hot}$';

xlabel('$x$','interpreter','latex');
ylabel('$y$','interpreter','latex');
I = 1:1:length(pos);
qq = quiver(pos(I,1),pos(I,2),v(I,1),v(I,2),8,'color',[0.1 0.2 0.2]);
axis([0 1 0 1])
hold off
% exportf(fig1, [imagepath 'DVD_2D.png'], 'format', 'png', 'color', 'rgb', 'Resolution', Resolution, 'Bounds', 'loose');
end
%% 2D Irreg
if 0
load([plotdatapath 'DVD_IMP_PHS']);
fig1=createFig('ps', 'm1 2x2 p4','scale',0.9);hold on;

%     plot(pos(:,1), pos(:,2),'k.','markersize',3)
I = 1:1:length(pos);
xp=scatter(pos(:,1),pos(:,2),50,T,'filled');
qq = quiver(pos(I,1),pos(I,2),v(I,1),v(I,2),3,'color',[0.1 0.2 0.2]);

colormap('jet')
cH = colorbar;
set(cH, 'YTick', [-1,1]);
cH.TickLabelInterpreter = 'latex';
cH.TickLabels{1} = '$T_{cold}$';
cH.TickLabels{end} = '$T_{hot}$';
xlabel('$x$','interpreter','latex');
ylabel('$y$','interpreter','latex');
axis equal
hold off
axis([0 1 0 1])
% exportf(fig1, [imagepath 'DVD_2D_irreg.png'], 'format', 'png', 'color', 'rgb', 'Resolution', Resolution, 'Bounds', 'loose');
end

%% 3D
if 0
load([plotdatapath 'DVD_IMP_PHS_3D_Ra6']);
fig1=createFig('ps', 'm1 4x2 p8','scale',1);
densityPlot(xp,yp,zp,vT,'numPatches',20,'alpha',0.5,'colormap','jet');hold on;

view([-25 25]);
I = 1:2:length(pos);
quiver3(pos(I,1),pos(I,2),pos(I,3),v(I,1),v(I,2),v(I,3),15,'color',[0.2 0.2 0.2]);
box on;
grid on;
xlabel('$x$','interpreter','latex');
ylabel('$y$','interpreter','latex');
zlabel('$z$','interpreter','latex');
axis([0 1 0 1 0 1]);
%     cH = colorbar;
%     set(cH, 'YTick', [1]);
%     cH.TickLabelInterpreter = 'latex';
%     cH.TickLabels{1} = '$T_{cold}$';
%     cH.TickLabels{end} = '$T_{hot}$';
% exportf(fig1, [imagepath 'DVD_3D.png'], 'format', 'png', 'color', 'rgb', 'Resolution', Resolution, 'Bounds', 'loose');
end
%% 3D irreg
if 1

load([plotdatapath 'DVD_IMP_PHS_3D']);

fig1=createFig('ps', 'm1 4x2 p8','scale',1);hold on

pos_(:,1) = reshape(xp, size(xp,1)*size(xp,1)*size(xp,1),1);
pos_(:,2) = reshape(yp, size(yp,2)*size(yp,2)*size(yp,2),1);
pos_(:,3) = reshape(zp, size(zp,3)*size(zp,3)*size(zp,3),1);
[i,d]=knnsearch(pos, pos_,'k',1);
II = d > 1.5*mean(d); % map
II = reshape(II, size(xp,1),size(yp,2),size(zp,3));
vT(II) = nan;

densityPlot(xp,yp,zp,vT,'numPatches',20,'alpha',0.5, 'colormap', 'jet');hold on;

alpha(0.2);

view([120 15]);
I = 1:1:length(pos);
quiver3(pos(I,1),pos(I,2),pos(I,3),v(I,1),v(I,2),v(I,3),15,'color',[0.2 0.2 0.2]);
box on;
grid on;
xlabel('$x$','interpreter','latex');
ylabel('$y$','interpreter','latex');
zlabel('$z$','interpreter','latex');
axis([0 1 0 1 0 1]);
% exportf(fig1, [imagepath 'DVD_3D_irreg.png'], 'format', 'png', 'color', 'rgb', 'Resolution', Resolution, 'Bounds', 'loose');
end

