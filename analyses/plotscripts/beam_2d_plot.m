prepare
casename = 'beam2d_holes';
casename = 'beam2d';

load([plotdatapath casename '.mat'])

f1 = setfig('b1', [600 300]);
f = 1e5;
scatter(x+f*u, y+f*v, 8, sv, 'filled');
daspect([1 1 1])
colorbar
colormap('jet')
caxis([0, 4e4])
caxis([0, 2e4])
ylim([-2 5])
xlim([0 16])
title(sprintf('von Mises stress, $N = %d$', N))


% exportf(f1, [imagepath casename '.png']);