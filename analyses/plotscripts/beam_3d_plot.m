prepare
casename = 'beam3d_holes';
casename = 'beam3d';

load([plotdatapath casename '.mat'])

f1 = setfig('b1', [600 400]); view(3)
f = 5e4;
scatter3(x+f*u, y+f*v, z+f*w, 10, sv, 'filled');
daspect([1 1 1])
colorbar
colormap('jet')
ylim([0 5])
xlim([0 16])
zlim([-3 2])
caxis([0 5e4])
title(sprintf('von Mises stress, $N = %d$', N))

% exportf(f1, [imagepath casename '.png']);