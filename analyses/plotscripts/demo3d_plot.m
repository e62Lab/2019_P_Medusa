prepare
casename = 'demo2d';
load([plotdatapath casename '.mat'])


f1 = setfig('b1', [600 400]);
scatter(x, y, 15, sol, 'filled');
contour(X, Y, V, linspace(0, 0.07, 30));
axis equal
colorbar
colormap jet
xlim([-2.2 2.5])
ylim([-1.5 2])

% exportf(f1, [imagepath casename '.pdf']);