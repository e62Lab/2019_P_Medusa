prepare

file = [datapath 'domains.h5'];

pos = h5read(file, '/domain1/pos');
x = pos(:, 1);
y = pos(:, 2);

types = h5read(file, '/domain1/types');
I = types > 0;
B = types < 0;

supp = h5read(file, '/domain1/supp')+1;

selected = [45, 240, 950];

f1 = setfig('b1', [600 400]);
scatter(x(I), y(I), 5, 'ok', 'filled');
scatter(x(B), y(B), 20, 'xk');

bmap = h5read(file, '/domain1/bmap')+1;
normals = h5read(file, '/domain1/normals');
bnd = bmap(B);
quiver(x(B), y(B), normals(bnd, 1), normals(bnd, 2), 0.3, 'Color', 0.5*[1 1 1]);

for i = selected
   scatter(x(i), y(i), 20, '*r'); 
   scatter(x(supp(i, :)), y(supp(i, :)), 20, 'ok');
end

title(sprintf('$N = %d$', length(x)));
axis equal
lgd = legend('interior nodes', 'boundary nodes', 'normals', 'selected nodes',...
       'support nodes');
set(lgd, 'Position', [0.1652    0.7552    0.2525    0.2310]);

% exportf(f1, [imagepath 'domain1.pdf']);